library(doParallel)
library(reshape2)
library(plyr)
library(foreach)
library(quantmod)
library(stringr)
library(doMC)

# enable 4-cores parallelism where applicable
registerDoMC(cores=4)

## Pseudo-command-line parameters
#
# We just use this to toggle at runtime between the normal EUMM dataset processing
# and the Bloomberg report dataset processing.
# By default, we process EUMM data. Set opt__do_bloomberg_data to TRUE to
# run Bloomberg code instead.

opt__do_bloomberg_data <- FALSE

## Contstants

# list of year to use in calculations
year_range_vars = sapply(seq(1994,2014), as.character)

# broad sectors analysis: sector groupings
broad_sectors <- list(
  HVAS = c('K', 'J', 'M', 'L', 'N'),
  MANUFACTURING = c('C'),
  PAEH = c('O', 'P', 'Q'),
  RT = c('G', 'H')
)

## Function definitions

#' Return list of sector names with GVA prepended, to be used as variable names
#' 
#' @param sector_list The list of individual sectors in the group
#' @return The list of variable names derived from the group's sector names
sector_list_names <- function(sector_list) {
  paste('GVA', sector_list, sep='')
}

#' Calculate absolute GVA values for a group of sectors
#' 
#' @param sector_list The list of individual sectors in the group
#' @return Sector GVA per city per year
broad_sector_raw_data <- function(sector_list, sector_name) {
  performance <- ddply(
    mr_raw_data[mr_raw_data$Indicator.Code %in% sector_list_names(sector_list), ],
    c('MetroRegion', 'year'),
    summarise,
    value=sum(value)
  )
  
  # add Indicator.Name column to enable rbind()
  performance$Indicator.Code <- paste('BROAD_SECTOR', sector_name, 'GVA', sep='_')
  
  performance
}

#' Calculate location quotient of a grouping of sectors
#'
#' @param sector_list The list of individual sectors in the group
#' @param year The year for which to compute LQs
#' @return The location quotient of the given sector group
sector_group_location_quotient <- function(sector_list, year) {
  sector_data <- broad_sector_raw_data(sector_list, 'sector')
  mr_sector_percent <-sector_data[sector_data$year == year, 'value'] / mr_raw_data[mr_raw_data$Indicator.Code == 'GVA' & mr_raw_data$year == year,'value']
  
  national_sector_group_data <- metro_region_list_with_national_data[metro_region_list_with_national_data$Indicator.Code %in% sector_list_names(sector_list), c('MetroRegion', 'Indicator.Code', year)]
  national_gva_data <- metro_region_list_with_national_data[metro_region_list_with_national_data$Indicator.Code == 'GVA', c('MetroRegion', 'Indicator.Code', year)]
  colnames(national_gva_data) = colnames(national_sector_group_data) = c('MetroRegion', 'Indicator.Code', 'value')
  # now summarise to collapse transborder MRs into a single value
  national_gva_data_summarised <- ddply(national_gva_data, "MetroRegion", numcolwise(sum))
  
  national_sector_value <- ddply(
    national_sector_group_data,
    c('MetroRegion'),
    summarise,
    value=sum(value)
  )
  
  national_sector_percent <- national_sector_value['value']/national_gva_data_summarised[,'value']
  
  location_quotient <- mr_sector_percent / national_sector_percent
  
  location_quotient[['value']]
}

#' Calculate economic sector percentage of total output for indicator
#' (EMP or GVA), for a given year
#' @param indicator EMP or GVA
#' @param year The year for which to calculate data
#' @return Tall list (like mr_raw_data) with sector percentages instead
#'         of absolute values
sector_percent <- function (indicator, year) {
  # we need variable names starting with indicator and having
  # at least one further character (e.g. /^EMP.+/)
  grep_pattern <- paste('^', indicator, '.+', sep = '')
  
  # extract raw data: only rows with individual sector values
  sectors_only <- mr_raw_data[grep(grep_pattern, mr_raw_data$Indicator.Code), ]
  
  sectors_only_in_given_year <- sectors_only[sectors_only$year == year, ]
  
  sector_percentages <- ddply(sectors_only_in_given_year, .(MetroRegion), summarise, Indicator.Code=Indicator.Code, year=year, value = value / sum(value) * 100)
  
  return(sector_percentages)
}

#' Calculate economic sector percentage of total output for indicator
#' (EMP or GVA), for a given year, with output in wide format ready
#' to be merged with final mr_data
#' @param indicator EMP or GVA
#' @param year The year for which to calculate data
#' @return Wide list (like full_eumm_data) with sector percentages instead
#'         of absolute values 
sector_percent_wide <- function (indicator, this_year) {
  tall <- sector_percent(indicator, this_year)
  
  wide <- dcast(tall, MetroRegion ~ Indicator.Code + year, value.var='value')
  
  widenames <- names(wide)
  
  names(wide) <- ifelse(grepl(paste('^', indicator, sep = ''), widenames), str_c('SECTOR_PERCENT_', widenames), widenames)
  
  return(wide)
}

# calculate year-on-year growth
yoyabs <- function(value, base) value / base * 100

# calculate aggregated Metro Region data - raw values
metro_area_raw_data <- function(dataset) {
  # melt
  d2 <- melt(dataset, id.vars=c("MetroRegion", "Indicator.Code"), measure.vars=year_range_vars, variable.name = "year", value.name="value", preserve.na = T)
  d2aggregated <- aggregate(value ~ MetroRegion + Indicator.Code + year, data=d2, sum, na.action=na.pass)
}

# calculate year-on-year growth and 100-based values
metro_area_computed_data <- function(raw_data, prefix='') {  
  # calculate yoy growth (TODO: need to ensure years are monotonic)
  g2 <- ddply(raw_data, .(MetroRegion, Indicator.Code), mutate, GR = Delt(value) * 100, .parallel=TRUE)
  
  # calculate year values relative to first year of data = 100 (TODO: need to ensure years are monotonic)
  v2 <- ddply(raw_data, .(MetroRegion, Indicator.Code), mutate, B100 = yoyabs(value, na.locf(value)[1]), .parallel=TRUE)
  
  # cast according to Indicator.Code and year
  g3 <- dcast(g2, MetroRegion ~ Indicator.Code + year, value.var="GR", sum)
  v3 <- dcast(v2, MetroRegion ~ Indicator.Code + year, value.var="B100")
  
  # append data type to column names
  # first get current names
  g3names <- names(g3)
  v3names <- names(v3)
  # then append suffix where current column name ends in '_YYYY'
  # using _GR suffix for growth, _B100 suffix for base=100
  names(g3) <- ifelse(grepl("_\\d\\d\\d\\d$", g3names), str_c(g3names, "_GR"), g3names)
  names(v3) <- ifelse(grepl("_\\d\\d\\d\\d$", v3names), str_c(v3names, "_B100"), v3names)
  
  # now merge all the data into a single data frame
  data <- merge(g3, v3)

  return (data)
}

eumm_data <- function(data) {
  ### data analysis
  
  # Calculations for the Bloomberg report
  # Average MR GDP growth rates - 2003 to 2013
  if(opt__do_bloomberg_data) {
    data$BLOOMBERG.AVG_GDP_GR_2003_2013 <- rowMeans(subset(data, select=GVA_2004_GR:GVA_2013_GR))
  }
  
  # Average Employment and GVA growth over 2001-2007 and 2008-2014 periods
  data$AVG_GDP_GR_2002.2007 <- rowMeans(subset(data, select=GVA_2002_GR:GVA_2007_GR), na.rm = T)
  data$AVG_EMP_GR_2002.2007 <- rowMeans(subset(data, select=EMP_2002_GR:EMP_2007_GR), na.rm = T)
  data$AVG_GDP_GR_2008.2014 <- rowMeans(subset(data, select=GVA_2008_GR:GVA_2014_GR), na.rm = T)
  data$AVG_EMP_GR_2008.2014 <- rowMeans(subset(data, select=EMP_2008_GR:EMP_2014_GR), na.rm = T)
  
  data$NATIONAL_AVG_GDP_GR_2002.2007 <- rowMeans(subset(data, select=NATIONAL_GVA_2002_GR:NATIONAL_GVA_2007_GR), na.rm = T)
  data$NATIONAL_AVG_EMP_GR_2002.2007 <- rowMeans(subset(data, select=NATIONAL_EMP_2002_GR:NATIONAL_EMP_2007_GR), na.rm = T)
  data$NATIONAL_AVG_GDP_GR_2008.2014 <- rowMeans(subset(data, select=NATIONAL_GVA_2008_GR:NATIONAL_GVA_2014_GR), na.rm = T)
  data$NATIONAL_AVG_EMP_GR_2008.2014 <- rowMeans(subset(data, select=NATIONAL_EMP_2008_GR:NATIONAL_EMP_2014_GR), na.rm = T)
  
  # Metropolitan GVA per capita vs National GVA per capita, 2008-2014
  # data$MR_VS_NATIONAL_GVA_PER_CAPITA_2008.2014 <- 
    
  # Resilience indicators

  ## Resilience1 - R1 - Ability to grow post-2008 recession
  # The sum of the annual growth rates of Employment and GVA for the 2008 to
  # 2012 period for each metropolitan region. This indicator tracks the economic
  # performance of metropolitan regions during the crisis. It measures resilience
  # as the ability to have high GVA and Employment growth rates despite the crisis.

  data$Resilience1_GVA <- data$AVG_GDP_GR_2008.2014
  data$Resilience1_EMP <- data$AVG_EMP_GR_2008.2014
  data$Resilience1 <- data$Resilience1_GVA + data$Resilience1_EMP

  ## Resilience2 - R2 - Difference of post-recessions vs pre-recession growth
  # The sum of the differences between the annual growth rates of Employment and
  # GVA for the 2008 to 2012 and the annual growth rates of the same indicators for
  # the 2002 to 2007 period, for each metropolitan region. This indicator measures
  # how different the GVA and Employment growth rates of metropolitan regions in
  # the recession period are from their values over the 2002 to 2007 period. It
  # thus assesses the resilience of a metropolitan region as the ability to
  # maintain its historical GVA and employment growth rates despite the recession.

  data$Resilience2_GVA <- data$AVG_GDP_GR_2008.2014 - data$AVG_GDP_GR_2002.2007
  data$Resilience2_EMP <- data$AVG_EMP_GR_2008.2014 - data$AVG_EMP_GR_2002.2007
  data$Resilience2 <- data$Resilience2_GVA + data$Resilience2_EMP

  ## Resilience3 - R3 - Metropolitan vs national performance
  # The sum of the differences between the metropolitan and national annual
  # growth rates of GVA and Employment over the 2008 to 2012 period. This indicator
  # gauges the degree to which a metropolitan region is over- or under-performing
  # its national context. It measures the resilience of a metropolitan region as
  # the ability to maintain its economic position within the national economy
  # despite the recession. To make sure that large metropolitan regions do not
  # affect national growth rates, metropolitan regions are taken out of the
  # national total to calculate their score on this indicator.
  data$Resilience3_GVA <- data$AVG_GDP_GR_2008.2014 - data$NATIONAL_AVG_GDP_GR_2008.2014
  data$Resilience3_EMP <- data$AVG_EMP_GR_2008.2014 - data$NATIONAL_AVG_EMP_GR_2008.2014
  data$Resilience3 <- data$Resilience3_GVA + data$Resilience3_EMP
  
  
  ## Broad sector analysis
  #
  data$BROAD_SECTOR_HVAS_AVG_GDP_GR_1998.2007 <- scale(rowMeans(subset(data, select=BROAD_SECTOR_HVAS_GVA_1998_GR:BROAD_SECTOR_HVAS_GVA_2007_GR), na.rm = T))
  data$BROAD_SECTOR_HVAS_AVG_GDP_GR_2008.2014 <- scale(rowMeans(subset(data, select=BROAD_SECTOR_HVAS_GVA_2008_GR:BROAD_SECTOR_HVAS_GVA_2014_GR), na.rm = T))
  data$BROAD_SECTOR_MANUFACTURING_AVG_GDP_GR_1998.2007 <- scale(rowMeans(subset(data, select=BROAD_SECTOR_MANUFACTURING_GVA_1998_GR:BROAD_SECTOR_MANUFACTURING_GVA_2007_GR), na.rm = T))
  data$BROAD_SECTOR_MANUFACTURING_AVG_GDP_GR_2008.2014 <- scale(rowMeans(subset(data, select=BROAD_SECTOR_MANUFACTURING_GVA_2008_GR:BROAD_SECTOR_MANUFACTURING_GVA_2014_GR), na.rm = T))
  data$BROAD_SECTOR_PAEH_AVG_GDP_GR_1998.2007 <- scale(rowMeans(subset(data, select=BROAD_SECTOR_PAEH_GVA_1998_GR:BROAD_SECTOR_PAEH_GVA_2007_GR), na.rm = T))
  data$BROAD_SECTOR_PAEH_AVG_GDP_GR_2008.2014 <- scale(rowMeans(subset(data, select=BROAD_SECTOR_PAEH_GVA_2008_GR:BROAD_SECTOR_PAEH_GVA_2014_GR), na.rm = T))
  data$BROAD_SECTOR_RT_AVG_GDP_GR_1998.2007 <- scale(rowMeans(subset(data, select=BROAD_SECTOR_RT_GVA_1998_GR:BROAD_SECTOR_RT_GVA_2007_GR), na.rm = T))
  data$BROAD_SECTOR_RT_AVG_GDP_GR_2008.2014 <- scale(rowMeans(subset(data, select=BROAD_SECTOR_RT_GVA_2008_GR:BROAD_SECTOR_RT_GVA_2014_GR), na.rm = T))
  
  return (data)
}

## Core script

# read dataset
if(opt__do_bloomberg_data) {
  raw_data <- read.csv("data/oxecon_raw_data_20140623.csv")
} else {
  raw_data <- read.csv("data/oxecon_raw_data_20140901.csv")
  # raw_data <- read.csv("data/oxecon_raw_data_20141114_02.csv")
}

# remove /EC$/ from GVA indicator names across the data frame
raw_data[, "Indicator.code"] <- gsub("^EMPTOT$", "EMP", raw_data[,"Indicator.code"])
raw_data[, "Indicator.code"] <- gsub("^EMPRES$", "EMP.ResidentialBased", raw_data[,"Indicator.code"])
raw_data[, "Indicator.code"] <- gsub("^GDPEC$", "GVA", raw_data[,"Indicator.code"])
raw_data[, "Indicator.code"] <- gsub("EC$", "", raw_data[,"Indicator.code"])

# read base Metro Region list
if(opt__do_bloomberg_data) {
  metro_region_list <- read.csv("data/MRs_bloomberg_report_2014-09_NUTS3_2006codes.csv")
} else {
  metro_region_list <- read.csv("data/MRs.csv")
}
# read MR geocode data
geocodes <- read.csv("data/MR_geocodes.csv")
# read MR capital city data
capital_cities <- read.csv("data/MR_capitals.csv")
# read MR city type data (according to the (2010 State of European Cities Report)[http://ec.europa.eu/regional_policy/sources/docgener/studies/pdf/urban/state_exec_en.pdf])
city_types <- read.csv("data/MR_city-types.csv")

# add column with national context
metro_region_list_with_national_context <- metro_region_list
colnames(metro_region_list_with_national_context) = c('MetroRegion', 'Countries')
metro_region_list_with_national_context$Countries <- sub('^(..).*$', '\\1', metro_region_list_with_national_context$Countries)
metro_region_list_with_national_context = unique(metro_region_list_with_national_context)
metro_region_list_with_national_context_label = aggregate(Countries ~ MetroRegion, metro_region_list_with_national_context, FUN = paste)
metro_region_list <- merge(metro_region_list, metro_region_list_with_national_context_label)

# just select the columns we need
# core_eumm_raw_data <- raw_data[c(1, 5:27)]  # old Oxford Economics data schema
core_eumm_raw_data <- raw_data[c(1, 7:27, 37, 38)]  # Oxford Economics data schema as of 2014-06

# apply our column names
colnames(core_eumm_raw_data) = c('Location.Name', year_range_vars, 'NUTS.Code', 'Indicator.Code')

# join Metro Region list with core raw data (by NUTS.Code), getting core raw data with extra column (MetroRegion)
core_mr_raw_data <- merge(metro_region_list, core_eumm_raw_data, by='NUTS.Code')

# given a list of Metro Regions with their national contexts, merge this with NUTS0 raw data
metro_region_list_with_national_data = merge(metro_region_list_with_national_context, core_eumm_raw_data, by.x='Countries', by.y='NUTS.Code')
# then sum all the same-indicator data for each MetroRegion
mrlwnd_molten <- melt(metro_region_list_with_national_data, id.vars=c("MetroRegion", "Indicator.Code"), measure.vars=year_range_vars, variable.name = "year", value.name="value")
mrlwnd_aggregated <- aggregate(value ~ MetroRegion + Indicator.Code + year, data=mrlwnd_molten, sum)
# and finally filter only variables we need
mrlwnd_aggregated_in_use <- mrlwnd_aggregated[which(mrlwnd_aggregated$Indicator.Code %in% c("GVA", "EMP", "POPTOTT")),]

# d1sub <- subset(core_mr_raw_data, grepl('^PT...$', NUTS.Code))
d1sub <- core_mr_raw_data

# remove Countries row to avoid error on melt (when did we put this column back in to start with? it's useless at this stage and can always be added at the end via merge)
d1sub$Countries <- NULL

mr_raw_data <- metro_area_raw_data(d1sub)

# add calculated broad sector GVA, for each sector group
performance_hvas <- broad_sector_raw_data(broad_sectors$HVAS, 'HVAS')
performance_manufacturing <- broad_sector_raw_data(broad_sectors$MANUFACTURING, 'MANUFACTURING')
performance_paeh <- broad_sector_raw_data(broad_sectors$PAEH, 'PAEH')
performance_rt <- broad_sector_raw_data(broad_sectors$RT, 'RT')

mr_raw_data <- rbind(mr_raw_data, performance_hvas, performance_manufacturing, performance_paeh, performance_rt)

# calculate average GVA per capita over post-recession timespan
avg_gva_2008.2014 <- rowMeans(subset(dcast(mr_raw_data[mr_raw_data$Indicator.Code == 'GVA' & as.numeric(as.character(mr_raw_data$year)) >= 2008 & as.numeric(as.character(mr_raw_data$year)) <= 2014, ], MetroRegion ~ Indicator.Code + year, value.var='value'), select=GVA_2008:GVA_2014), na.rm = T)
avg_pop_2008.2014 <- rowMeans(subset(dcast(mr_raw_data[mr_raw_data$Indicator.Code == 'POPTOTT' & as.numeric(as.character(mr_raw_data$year)) >= 2008 & as.numeric(as.character(mr_raw_data$year)) <= 2014, ], MetroRegion ~ Indicator.Code + year, value.var='value'), select=POPTOTT_2008:POPTOTT_2014), na.rm = T)
avg_mr_gva_per_capita_2008.2014 <- avg_gva_2008.2014 / avg_pop_2008.2014

mr_data <- metro_area_computed_data(mr_raw_data)
  
# add columns needed to calculate [population, GVA, GVA per capita] dominance
# TODO: these operations should be moved to a separate function accepting
# as a parameter the year for which the calculation should be made.
mr_data$POP_2007 <- mr_raw_data[which(mr_raw_data$Indicator.Code == 'POPTOTT' & mr_raw_data$year == 2007),'value']
mr_data$ABS_GDP_2007 <- mr_raw_data[which(mr_raw_data$Indicator.Code == 'GVA' & mr_raw_data$year == 2007),'value']
mr_data$POP_DOMINANCE_2007 <- cut(mr_data$POP_2007, breaks = c(0, 500, 1000, 2000, 3000, 4000, 5000, 10000, Inf), labels = seq(8,1), right = TRUE)
mr_data$GDP_DOMINANCE_2007 <- cut(mr_data$ABS_GDP_2007, breaks = c(0, 10000, 2500, 50000, 80000, 100000, 150000, 200000, Inf), labels = seq(8,1), right = TRUE)
# MR vs National GVA per capita dominance
national_raw_gva_pop_data <- dcast(mrlwnd_aggregated_in_use[mrlwnd_aggregated_in_use$Indicator.Code == 'GVA' | mrlwnd_aggregated_in_use$Indicator.Code == 'POPTOTT', ], MetroRegion ~ Indicator.Code + year, value.var="value")
national_avg_gva_per_capita_2008.2014 <- rowMeans(subset(national_raw_gva_pop_data, select=GVA_2008:GVA_2014), na.rm = T) / rowMeans(subset(national_raw_gva_pop_data, select=POPTOTT_2008:POPTOTT_2014), na.rm = T)
# this breaks Bloomberg calculations - leave it out while doing Bloomberg processing
if(!opt__do_bloomberg_data) {
  mr_data$GVA_PER_CAPITA_MR_VS_NATIONAL <- cut(avg_mr_gva_per_capita_2008.2014/national_avg_gva_per_capita_2008.2014, breaks = c(0, 0.663, 0.80, 1.00, 1.25, 1.50, Inf), labels = seq(6,1), right = TRUE)
}
# delete raw data that shouldn't be disclosed publicly having been proprietarised
mr_data$ABS_GDP_2007 <- NULL

## Broad sectors analysis
mr_data$LOCATION_QUOTIENT_HVAS_1998 <- scale(sector_group_location_quotient(broad_sectors$HVAS, 1998))
mr_data$LOCATION_QUOTIENT_MANUFACTURING_1998 <- scale(sector_group_location_quotient(broad_sectors$MANUFACTURING, 1998))
mr_data$LOCATION_QUOTIENT_PAEH_1998 <- scale(sector_group_location_quotient(broad_sectors$PAEH, 1998))
mr_data$LOCATION_QUOTIENT_RT_1998 <- scale(sector_group_location_quotient(broad_sectors$RT, 1998))

# this one is for Bloomberg - delete column once value is calculated
if(opt__do_bloomberg_data) {
  ABS_GDP_2013 <- mr_raw_data[which(mr_raw_data$Indicator.Code == 'GVA' & mr_raw_data$year == 2013),'value']
  POP_2013 <-  mr_raw_data[which(mr_raw_data$Indicator.Code == 'POPTOTT' & mr_raw_data$year == 2013),'value']
  mr_data$BLOOMBERG.GDP_PER_CAPITA_2013 <- ABS_GDP_2013 / POP_2013
}

# add raw population data
## first, get the population data only from raw mr data
mr_raw_data_population_only <- mr_raw_data[which(mr_raw_data$Indicator.Code == 'POPTOTT'), ]
## then cast to one column per year
mr_population_data <- dcast(mr_raw_data_population_only, MetroRegion ~ Indicator.Code + year, value.var="value")
## and merge into main dataset
mr_data <- merge(mr_data, mr_population_data, by='MetroRegion')

national_data <- metro_area_computed_data(mrlwnd_aggregated_in_use)
national_data_colnames <- colnames(national_data)
national_data_colnames[c(2:length(national_data_colnames))] <- sub("^", "NATIONAL_\\1", national_data_colnames[c(2:length(national_data_colnames))])
colnames(national_data) <- national_data_colnames

data <- merge(mr_data, national_data, by = "MetroRegion")

# compute our own indicators
full_eumm_data <- eumm_data(data)

# sector percent calculations
sector_emp_pc_1998 <- sector_percent_wide('EMP', 1998)
sector_gva_pc_1998 <- sector_percent_wide('GVA', 1998)
sector_emp_pc_2007 <- sector_percent_wide('EMP', 2007)
sector_gva_pc_2007 <- sector_percent_wide('GVA', 2007)
sector_emp_pc_2014 <- sector_percent_wide('EMP', 2014)
sector_gva_pc_2014 <- sector_percent_wide('GVA', 2014)
# and merge into main dataset
full_eumm_data <- merge(full_eumm_data, sector_emp_pc_1998, by = "MetroRegion")
full_eumm_data <- merge(full_eumm_data, sector_gva_pc_1998, by = "MetroRegion")
full_eumm_data <- merge(full_eumm_data, sector_emp_pc_2007, by = "MetroRegion")
full_eumm_data <- merge(full_eumm_data, sector_gva_pc_2007, by = "MetroRegion")
full_eumm_data <- merge(full_eumm_data, sector_emp_pc_2014, by = "MetroRegion")
full_eumm_data <- merge(full_eumm_data, sector_gva_pc_2014, by = "MetroRegion")

if(! opt__do_bloomberg_data) {
  # add geocode data
  full_eumm_data <- merge(full_eumm_data, geocodes, by="MetroRegion")
  # add capital city data
  full_eumm_data <- merge(full_eumm_data, capital_cities, by="MetroRegion")
  # add city type data
  full_eumm_data <- merge(full_eumm_data, city_types, by="MetroRegion")
}

# add back Countries column
full_eumm_data <- merge(full_eumm_data, metro_region_list_with_national_context_label, by = "MetroRegion")
# and make it a string
full_eumm_data$Countries <- sapply(full_eumm_data$Countries, FUN = paste, collapse = ',')

# round to two decimal digits for a smaller data file
rounded_full_eumm_data <- data.frame(lapply(full_eumm_data, function(y) if(is.numeric(y)) round(y, 2) else y))

if(opt__do_bloomberg_data) {
  bloomberg_data <- rounded_full_eumm_data[,c('MetroRegion', 'BLOOMBERG.AVG_GDP_GR_2003_2013', 'BLOOMBERG.GDP_PER_CAPITA_2013')]
}
