# European MetroMonitor - data analysis

This package processes the source dataset for LSE Cities' *European
MetroMonitor* research project, preparing it for use in the project's web
application.

