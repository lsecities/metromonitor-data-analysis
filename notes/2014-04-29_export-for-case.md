---
layout: post
title: Exporting data for CASE
date: 2014-04-29 17:30
---
Some colleagues from CASE asked us if we could provide them some data from the
European MetroMonitor analysis for cities they are focusing on as part of one
of their research projects: they needed growth rates data, and the data used in
most of our initial charts.

To extract the data they needed, I basically just applied a filter() to the
live JSON dataset within Chromium's Dev Tools, then applied a map() to flatten
out the resulting object, only extracting the attributes:

    _.map(
      _.filter(dataset.features, function(item) {
         return item.id == "Belfast" ||
                item.id == "Bilbao" ||
                item.id == "Leipzig - Halle" ||
                item.id == "Lille Transborder MR" ||
                item.id == "Lyon - St. Etienne MR" || 
                item.id == "Leeds - Bradford - Sheffield MR" || 
                item.id == "Turin"; }),
      function(item) { 
        return [ item.properties ];
      }
    );


Now, this is a bit crude, but works fine for a very small set of cities like
the ones in our colleagues' request. To save the resulting JSON, i used the
function mentioned here: https://stackoverflow.com/a/19818659/550077.

It is great that other projects are interested in our analysis up to the point
of asking for our processed data; ideally, our app should actually offer the
possibility to easily perform this kind of subsetting directly - of course it
is possible to inspect the Javascript environment, but without knowing the
internals of the app it's quite laborious to figure out the internals,
especially as the live app obviously serves minified JS code.

Hopefully, once the new data management scripts are fully equivalent to the
legacy ones, we should be able to easily add such a feature to the live app.
